"""
Responsible to copy the Odoo Database into a Postgres server based on the configuration inside the database_copy.yml file
"""

import argparse
import yaml

from src.Sql import Sql
from src.Client import Client


# Parse database_copy.yml


######## Hardcoded parameters ############

def retrieve_data(sql, to_be_copied, to_get):
    # Create and populate basic tables
    for table, fields in to_be_copied.items():

        print(f"Copying table {table}{'.' * (50 - len(table))}")

        if to_get:
            if table not in to_get:
                continue

        # Extract eventual condidtions and remove this from fields
        if '__or_conditions' in fields:
            conditions = ['|']*(len(fields['__or_conditions']) - 1) + [cond.split(" ") for cond in fields['__or_conditions']]
        else:
            conditions = []
        fields = {key: value for key,value in fields.items() if key[:2] != '__'}

        # Check which field can be used for time
        try:
            sync_time_field = [key for key,value in fields.items() if value == 'SYNC TIMESTAMP'][0]
        except IndexError:
            sync_time_field = None

        # Translate fields type from app types to Postgres types
        prepared_fields = {key: 'INT' if value=='EXT' else value for key,value in fields.items()}
        prepared_fields = {key: 'TIMESTAMP' if value=='SYNC TIMESTAMP' else value for key,value in prepared_fields.items()}
        
        # Create table
        new_columns = sql.create_table(table, prepared_fields)

        # If new columns:
        if new_columns:
            # Start by retrieving data for these specific columns
            data = Client().get(
                table, [],
                fields = ['id'] + new_columns,
                cache=False
            )

            # Then update table with new data
            sql.populate(table, 'id', [
                {
                    i: line[i][0] if fields[i] == 'EXT' else line[i] for i in line
                } for line in data
            ])

        # Get last value recorded:
        if sync_time_field:
            last_value = sql.get_last_value(table, sync_time_field)
        else:
            last_value = None

        if last_value:
            conditions.append([sync_time_field, '>', last_value])

        # Retrieve new data
        data = Client().get(
            table, conditions,
            fields=list(fields.keys()),
            cache=False
        )

        def pop(val, type):
            if val == False and type != 'BOOL':
                return "NULL"
            elif type == 'EXT':
                return val[0]
            else:
                return val

        sql.populate(table, 'id', [
            {
                i: pop(line[i], fields[i]) for i in line
            } for line in data
        ])

        print("Ok")


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--get", "-g", help="Specify the tables to retrieve. Otherwise, retrieve all", nargs='+')
    args = parser.parse_args()

    with open("cfg/credentials.yml") as fd:
        credentials = yaml.load(fd.read())

    # read config file
    with open('cfg/database_copy.yml', 'r') as fd:
        config = yaml.load(fd.read())

    sql = Sql(
        credentials['database']['host'],
        credentials['database']['port'],
        credentials['database']['user'],
        credentials['database']['pwd']
    )

    print("Trying to connect to Odoo Server")
    client = Client(credentials["odoo"])

    print("Connected to Odoo")
    retrieve_data(sql, config['tables'], args.get)
    