
import numpy as np
import matplotlib.pyplot as plt

def ca_per_year(sql):

    print("Generating CA per year figure !")

    query = """
        SELECT ca FROM (
                SELECT sum / inflation as ca FROM (
                    SELECT create_date, sum(ca) FROM "analysis.raw_data.product.month" 
                    WHERE create_date >= '2023-01-01' AND create_date < '2023-03-01' 
                    GROUP BY create_date
                ) as temp 
                INNER JOIN "analysis.inflation.month" ON "analysis.inflation.month".create_date = temp.create_date 
                ORDER BY temp.create_date
            ) as temp2;
    """

    data = [i for i in sql.execute_raw_sql(query)]
    print(data)
    plt.plot([0,1], [0,1])


    plt.savefig('reports/ca_per_year.pdf')

