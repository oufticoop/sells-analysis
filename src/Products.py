

from src.Client import Client

class ProductsClass():

    def __init__(self):
        
        # Get translation to get real name of products
        translation = {
            i['source']: i['value'] for i in 
            Client().get('ir.translation', [['name', '=', 'product.template,name']], fields=['source', 'value'])
        }

        # Get all order lines
        self.categories = {
            i['id']: {'name': i['name']}
            for i in Client().get('product.category', [], fields=['name'])
        }

        # for i in Client().get('product.product', [], fields=['display_name', 'categ_id']):
        #     print(i)
        self.products = {
            i['id']: {
                'name': translation.get(i['name'], i['name']), 
                'cat': i['categ_id'][0],
                'uom_po_id': i['uom_po_id'][0],
                'supplier': i['main_supplierinfo'][1] if i['main_supplierinfo'] else 'unknown'
            }
            for i in Client().get(
                'product.product', 
                ['|', ('active','=',True),  ('active','=',False)], 
                fields=['name', 'categ_id', 'uom_po_id', 'main_supplierinfo']
            )
        }
        pass
products = None

def Products():
    global products
    if not products:
        products = ProductsClass()
    
    return products