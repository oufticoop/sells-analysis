
import json
from typing import List
from xmlrpc import client as rpc_client
from pathlib import Path
from os import listdir
import yaml
import uuid

_CACHE_PATH = '.cache'

class ClientClass():

    def __init__(self, credentials, offline=False, verbose=False):

        self._url = f'{credentials["host"]}/xmlrpc/2'
        self._db = credentials['db']
        self._password = credentials['pwd']
        self._username = credentials['user']
        self._verbose = verbose

        if not offline:
            common = rpc_client.ServerProxy(f'{self._url}/common')
            self._uid = common.authenticate(self._db, self._username, self._password, {})
            self._models = rpc_client.ServerProxy(f'{self._url}/object')

    def get(self, table, conditions, fields=[], order_by=False, limit=False, cache=True):

        if self._verbose:
            print(f"Get operation: {table}, {conditions}, {fields}, {order_by}, {limit}, {cache}")

        Path(_CACHE_PATH).mkdir(exist_ok=True)
        req_str = '|'.join([str(i) for i in [table, conditions, fields, order_by, limit]])

        # Check if request already done. If so, get cached data
        if cache:
            for file in [f for f in listdir(_CACHE_PATH) if table in f]:
                with open(f'{_CACHE_PATH}/{file}') as fd:
                    # Read first line to check whether it matches the request
                    read_content = fd.readline()
                    if read_content[:-1] == req_str:
                    # If so, read the json package and return
                        return json.loads(fd.read())

        # If here, no file matched the request or cache disabled...
        result = self._models.execute_kw(
            self._db, self._uid, self._password, table, 
            'search_read', [conditions], {'fields': fields, 'order': order_by, 'limit': limit}
        )

        # Save request in file
        with open(f'{_CACHE_PATH}/{table}_{uuid.uuid1()}', 'w') as fd:
            fd.write(req_str)
            fd.write('\n')
            fd.write(json.dumps(result))

        return result

client = None

def Client(*args, **kwargs):

    global client

    if not client:
        client = ClientClass(*args, **kwargs)
    return client
