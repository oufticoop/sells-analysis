
from datetime import date

from src.Client import Client
from src.Products import Products
import src.utils as utils

class Purchases():

    def __init__(self, periods):

        # Get all purchase unity of measure
        self._uom = {i['id']: i['factor_inv'] for i in Client().get('uom.uom', [], fields=['factor_inv'])}

        # Here, we must have the entire history, otherwise we could miss old purchases of products we selled in the period
        self._purchases = Client().get('purchase.order.line', [
            ['date_order', '<', periods[-1]]
        ], fields=['product_id', 'price_subtotal', 'qty_received', 'product_uom_qty', 'price_unit', "date_order"],
        order_by="date_order")


    def stats_for_period(self, period_start, period_end):
        mean_prices = {}
        volumes = {}

        products_data = {}

        for line in utils.LinesInPeriods(self._purchases, period_start, period_end, "date_order"):

            with open("date_orders.txt", "a") as fd:
                fd.write(f"{line['date_order']}\n")
            product_id = line['product_id'][0]

            # TODO: here, uom linked to product, below (get_last_price) uom linked to line !
            qty = line['qty_received']*self._uom[Products().products[line['product_id'][0]]['uom_po_id']]
            real_price = line['price_unit']*line['qty_received']
            if product_id in products_data:
                products_data[product_id][0] += real_price
                products_data[product_id][1] += qty
            else:
                products_data[product_id] = [real_price, qty]

        mean_prices = {prod: (data[0] / data[1]) for prod, data in products_data.items() if data[1]}
        volumes = {prod: data[1] for prod, data in products_data.items()}

        return volumes, mean_prices

    def get_last_price(self, product, last_date):

        for line in reversed(self._purchases):
            if (line["date_order"] < last_date) and (line["product_id"][0] == product):
                try:
                    #self._uom[Products().products[line['product_id'][0]]['uom_po_id']]
                    return line["price_subtotal"]/line["product_uom_qty"]
                except ZeroDivisionError:
                    if(line['price_subtotal'] == 0):
                        return 0
                    else:
                        raise ZeroDivisionError()

        raise RuntimeError(f"No last price found for product {product} ({Products().products[product]})")
