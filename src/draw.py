from matplotlib.pyplot import *
import numpy as np 

with open('analyse_marge_par_fournisseur.csv') as fd:
    data = [i.split(';') for i in fd.read().split('\n')]

data = data[6:35]

labels = list(map(lambda x: x if len(x) < 30 else (x[0:27] + '...'), [i[0] for i in data]))
print(labels)

fig, ax1 = subplots()

X_axis = range(0, len(data)*2, 2)

ax1.bar(
    [i - 0.25 for i in X_axis],
    [float(i[1]) for i in data],
    0.4,
    tick_label = labels,
    label = "Chiffre d'affaires", 
    color='#4E71BF'
)

ax2 = ax1.twinx()

ax2.bar([i + 0.25 for i in X_axis], [float(i[3]) for i in data], 0.4, label = 'Marge réelle', color='#ED7D31')
ax2.plot([-2, len(data)*2+1], [1.2, 1.2], color='green')
ax2.plot([-2, len(data)*2+1], [1, 1], color='red')
ax2.set_xlim(-2, len(data)*2)
ax2.set_ylim(top=1.3)

for tick in ax1.get_xticklabels():
    tick.set_rotation(90)

fig.legend()
title("Chiffre d'affaires et marge réelles par producteur pour les 20 producteurs avec la marge la plus faible")
show()