
from datetime import date, timedelta
import math
import sys
import re

from src.Stock import Stock
from src.Purchases import Purchases
from src.Products import Products
from src.Sales import Sales

from src.Client import Client

# for cat, amount in Sales().total_per_category('2021-01-01', '2021-02-01').items():
#     print(f'{Products().categories[cat]["name"]}: {amount}')

# Start date: mardi 5/1
# Stop date: mardi 2/2

class Controller:

    def __init__(self):
        self.prod_ignore = []
        # Read  Product Ignore file
        with open('cfg/prodignore') as fd:
            for line in fd.read().split('\n'):
                if re.compile('^\d+$').match(line):
                    self.prod_ignore.append(int(line))


    def prepare_analysis(self, start, end, period):
        self.start_date = date.fromisoformat(start)
        self.end_date = date.fromisoformat(end)
        if period:
            self.period_length = timedelta(days=period)

            self.periods = []
            current_date = self.start_date
            while current_date <= self.end_date:
                self.periods.append(current_date.isoformat())
                current_date += self.period_length

        else:
            self.periods = [start, end]

        self.sales = Sales(self.periods)
        self.stock_variation = Stock(self.periods)
        self.purchases = Purchases(self.periods)


    def analyse_per_product(self, start, end, period):

        self.prepare_analysis(start, end, period)

        start_stop = (self.periods[0], self.periods[1])

        vars_no_purchase, _ = self.stock_variation.stock_mvt_in_period(*start_stop)
        _, means = self.purchases.stats_for_period(*start_stop)

        self.analysis_per_product = {}

        for prod, result in self.sales.total_per_product(*start_stop).items():
            
            # If product should be ignore, don't execute analysis
            if prod in self.prod_ignore:
                continue

            var_no_pur = vars_no_purchase.get(prod, 0)
            try:
                mean = means[prod]
            except KeyError:
                try:
                    mean = self.purchases.get_last_price(prod, start_stop[0])
                except RuntimeError:
                    print(f"ERROR: No last price for {Products().products[prod]['name']}")

            try:
                marge = result / ( - var_no_pur*mean )
            except ZeroDivisionError:
                if result != 0:
                    #raise RuntimeError(f"Unexpected division by 0 for {prod} ({Products().products[prod]['name']}) (period: {start_stop})")
                    print(f"Unexpected division by 0 for {prod} ({Products().products[prod]['name']}) (period: {start_stop})", file=sys.stderr)
                marge = math.nan

            self.analysis_per_product[prod] = (marge, result, -var_no_pur*mean)
        
        return self.analysis_per_product

    def analyse_per_category(self, start, end, period):

        analysis = self.analyse_per_product(start, end, period)

        self.analysis_per_category = {}

        for prod, result in analysis.items():

            # Determine category
            category = Products().products[prod]['cat']

            if category not in self.analysis_per_category:
                self.analysis_per_category[category] = [0,0,0]

            # Update statistics
            self.analysis_per_category[category][1] += result[1]
            self.analysis_per_category[category][2] += result[2]

        for cat in self.analysis_per_category:
            # Compute statistics
            self.analysis_per_category[cat][0] = self.analysis_per_category[cat][1] / self.analysis_per_category[cat][2]

        return self.analysis_per_category

    def mean_ca_per_day(self, start, end, period):

        self.prepare_analysis(start, end, period)

        result = {}
        for i in range(len(self.periods) - 1):
            start = self.periods[i]
            end = self.periods[i+1]
            result[start] = self.sales.get_mean_sales_per_day_of_week(start, end)

        return result