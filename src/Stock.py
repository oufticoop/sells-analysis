
from datetime import date

from src.Client import Client
from src.Products import Products
import src.utils as utils

_WH_STOCK_ID = 12
_VENDORS_LOCATION_ID = 8

class Stock():

    _stock_mvt = {}

    def __init__(self, periods):

        # Get all mouvements
        self._to_lines = Client().get('stock.move.line', [
            ['write_date', '>=', periods[0]],
            ['write_date', '<', periods[-1]],
            ['location_dest_id', '=', _WH_STOCK_ID]
            ], fields=['product_id', 'qty_done', "write_date", 'location_id'], order_by="write_date")

        self._from_lines = Client().get('stock.move.line', [
            ['write_date', '>=', periods[0]],
            ['write_date', '<', periods[-1]],
            ['location_id', '=', _WH_STOCK_ID]
            ], fields=['product_id', 'qty_done', "write_date"], order_by="write_date")

    def stock_mvt_in_period(self, period_start, period_end):

        stock_mvt_no_purchase = {}
        stock_mvt_purchase_only = {}

        def loop_over(lines, sign):
            for line in utils.LinesInPeriods(lines, period_start, period_end, "write_date"):

                if sign == 1 and line['location_id'][0] == _VENDORS_LOCATION_ID:
                    table = stock_mvt_purchase_only
                else:
                    table = stock_mvt_no_purchase

                prod_id = line['product_id'][0]
                if prod_id not in table:
                    table[prod_id] = 0
        
                table[prod_id] += sign * line['qty_done']

        loop_over(self._from_lines, -1)
        loop_over(self._to_lines, 1)

        return (stock_mvt_no_purchase, stock_mvt_purchase_only)
            