"""
Responsible to copy the Odoo Database into a Postgres server based on the configuration inside the database_copy.yml file
"""

import argparse
import yaml
import datetime

from src.Sql import Sql
from src.Client import Client

def analyse(sql, analysis_list, analysis_config):
    # Create and populate analysis tables:

    analysis_to_perform = {key: value for key, value in analysis_list.items() if not analysis_config or key in analysis_config}

    for table, fields in [(key, value['fields']) for key, value in analysis_to_perform.items()]:

        print(f"Create table for {table}...{' ' * (50 - len(table))}", end="")
        
        # Create table (if it doesn't exist already)
        sql.create_table(table, fields)

        # Empty table (check for a cleaner way to do this)
        sql.execute_raw_sql(f"""
            DELETE FROM "{table}";
        """, commit=True)

        print("Ok")

    # Execute SQL queries and scripts
    for name,analysis in analysis_to_perform.items():
        print(f"Execute query for {name}...{'.' * (49 - len(name))}", end="")
        if 'query' in analysis:
            sql.execute_raw_sql(analysis['query'], commit=True)
        elif 'python' in analysis:
            execute_python(sql, analysis['python'])
        
        print("Ok")
    
def execute_python(sql, script):
    func_name = script['name']
    parameters = [sql.execute_raw_sql(i).fetchall() for i in script['parameters'].values()]

    # Call function
    returned = globals()[func_name](*parameters)

    # INSERT value into table
    sql.populate(script['return'], None, returned)

def determine_purchase_per_period(products, periods, price_per_period, price_per_day):
    result = []

    list_products = [i[0] for i in products]
    list_period = [i[0] for i in periods]

    _per_period = {(i[0], i[1]): (i[2], i[3]) for i in price_per_period}
    _per_day = {prod: [(i[0], i[2], i[3]) for i in price_per_day if i[1] == prod] for prod in list_products}
    
    # For each product for each month, look for valid data
    for period in list_period:
        for product in list_products:
            if (period, product) in _per_period and _per_period[(period, product)][0]:
                    result.append({
                        'create_date': period.strftime("%Y-%m-%d"),
                        'product_id': product,
                        'qty': _per_period[(period, product)][0],
                        'price': _per_period[(period, product)][1] if _per_period[(period, product)][1] else 0
                    })
            # Otherwise, look for last value
            else:
                for date, qty, price in _per_day[product]:

                    if date.replace(tzinfo=datetime.timezone.utc) < period.replace(tzinfo=datetime.timezone.utc):
                        if qty:
                            result.append({
                                'create_date': period.strftime("%Y-%m-%d"),
                                'product_id': product,
                                'qty': qty,
                                'price': price if price else 0
                            })
                            break
                        
    return result

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--analysis", "-a", help="Specify an analysis to perform. Otherwise, perform all", nargs='+')
    args = parser.parse_args()

    with open("cfg/credentials.yml") as fd:
        credentials = yaml.load(fd.read())

    # read config file
    with open('cfg/database_analysis.yml', 'r') as fd:
        config = yaml.load(fd.read())

    sql = Sql(
        credentials['database']['host'],
        credentials['database']['port'],
        credentials['database']['user'],
        credentials['database']['pwd']
    )
        
    analyse(sql, config['analysis'], args.analysis)
