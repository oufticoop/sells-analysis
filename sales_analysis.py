

import argparse

import matplotlib.pyplot as plt
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
#import numpy as np

import yaml

from src.Sql import Sql


def plot(config, sql, plot_list):

    for title, graph in config.items():

        # Check if graph should be drawn
        if plot_list and title not in plot_list:
            continue

        print(f"Plotting: {title}")

        dataset = []

        csv_header = [graph['absysse']]
        csv_data = {}

        for data_name, data_cfg in graph['dataset'].items():
            query_result = sql.execute_raw_sql(data_cfg['query']).fetchall()
            dataset += [{graph['absysse']: i[0], 'Légende': data_name, 'Métrique': i[1]} for i in query_result]

            # To generate CSV file
            csv_header.append(data_name)
            for data in query_result:
                if data[0] in csv_data:
                    csv_data[data[0]][data_name] = data[1]
                else:
                    csv_data[data[0]] = {data_name: data[1]}

        if graph['type'] == 'line':
            fig = px.line(dataset, x=graph['absysse'], y="Métrique", color='Légende')
        elif graph['type'] == 'bar':
            # fig = px.bar(dataset, x=graph['absysse'], y='Métrique', color='Légende', barmode='group')
            fig = make_subplots(specs=[[{"secondary_y": True}]])
            i = 0
            for data_name, data_cfg in graph['dataset'].items():
                i += 1
                data = sql.execute_raw_sql(data_cfg['query']).fetchall()
                # fig.add_trace(
                #     go.Bar(x=[i[0]  for i in data], y=[i[1] for i in data], name=data_name),
                #     secondary_y=data_cfg.get('secondary_axis', False)
                # )
                fig.add_trace(
                    go.Bar(x=[i[0]  for i in data], y=[i[1] for i in data], name=data_name, offsetgroup=i), row=1, col=1,
                    secondary_y=data_cfg.get('secondary_axis', False)
                )

            fig.update_layout({
                'barmode':'group',
                'yaxis2':{'tickvals':[0.5, 1, 1.2]}
            })
            fig.update_xaxes(tickangle=-45)

        for name, line in graph.get('h_ref_line', {}).items():
            if 'query' in line:
                value = sql.execute_raw_sql(line['query']).fetchall()[0][0]
            elif 'value' in line:
                value = line['value']
            fig.add_hline(y=value, annotation_text=name, yref='y2' if line.get('secondary_axis', False) else 'y', line_color=line.get('color', 'black'))

            # fig.add_shape(type="line", yref="y2",
            #     x0=0, y0=value, x1=20, y1=value,
            #     line=dict(
            #         color="red",
            #         dash="dash"
            #     ),
            # )
        # fig.show()
        image_title = title.replace(' ', '_').replace("'", '_')
        fig.write_image(f"{image_title}.pdf", engine="kaleido")

        # Copy data to a .csv file
        with open(f'results/{image_title}.csv', 'w') as fd:
            fd.write(",".join(csv_header))
            fd.write("\n")

            for x, data in csv_data.items():
                line = [x] + [data.get(i, '') for i in csv_header[1:]]
                fd.write(";".join([str(i) for i in line]))
                fd.write("\n")

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--plot", "-p", help="Specify plots to draw. Otherwise, plot all", nargs='+')
    args = parser.parse_args()

    # read config file
    with open('cfg/credentials.yml', 'r') as fd:
        cred = yaml.load(fd.read())

    sql = Sql(cred['database']['host'], cred['database']['port'], cred['database']['user'], cred['database']['pwd'])

    # read config file
    with open('cfg/plot.yml', 'r') as fd:
        config = yaml.load(fd.read())

    plot(config, sql, args.plot)