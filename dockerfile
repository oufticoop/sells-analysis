FROM postgres:12.12-alpine

RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python 
RUN python3 -m ensurepip && pip3 install --no-cache --upgrade pip setuptools

# Add psycopg2 stuff
# RUN apk add libpq-dev gcc 
RUN pip3 install psycopg2-binary PyYAML==5.3.1

# Copy repository content to container
COPY . /bin/sales-analysis