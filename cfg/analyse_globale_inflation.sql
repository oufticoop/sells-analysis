SELECT sum(inflation * ca) / sum(ca) FROM (
  SELECT purchase_2023.product_id, unit_price_2023 / unit_price_2022 as inflation FROM (
    SELECT 
      product_id, 
      sum(price_unit * qty_received) / sum(qty_received * factor_inv) as unit_price_2022
    FROM "purchase.order.line" LEFT JOIN "uom.uom" ON product_uom = "uom.uom".id 
    WHERE qty_received > 0 AND "purchase.order.line".create_date >= '2022-01-01' AND "purchase.order.line".create_date < '2023-01-01'
    GROUP BY product_id
  ) as purchase_2022
  INNER JOIN
  (
    SELECT 
      product_id, 
      sum(price_unit * qty_received) / sum(qty_received * factor_inv) as unit_price_2023
    FROM "purchase.order.line" LEFT JOIN "uom.uom" ON product_uom = "uom.uom".id 
    WHERE qty_received > 0 AND "purchase.order.line".create_date >= '2023-01-01' AND "purchase.order.line".create_date < '2024-01-01'
    GROUP BY product_id
  ) as purchase_2023
  ON purchase_2022.product_id = purchase_2023.product_id
) as inflation
INNER JOIN
(
  SELECT 
    product_id,
    sum(ca) as ca 
  FROM "analysis.ca.product" where create_date > '2023-01-01' GROUP BY product_id
) as ca_2023
ON inflation.product_id = ca_2023.product_id;
