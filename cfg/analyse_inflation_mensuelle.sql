
SELECT 
  sum("analysis.raw_data.product.month".ca * product_inflation.inflation) / sum("analysis.raw_data.product.month".ca) 
FROM 
  "analysis.raw_data.product.month"
INNER JOIN
(
  SELECT * FROM 
    (SELECT 
      all_prices.product_id,
      all_prices.create_date,
      CASE WHEN ref_prices.unit_price > 0 THEN
        all_prices.unit_price / ref_prices.unit_price
      ELSE
        'NaN'::NUMERIC
      END as inflation
    FROM
      (SELECT * FROM "analysis.unit_price.month" ) as all_prices
    INNER JOIN
      (SELECT * FROM "analysis.unit_price.month" WHERE create_date = '2021-01-01') as ref_prices
    ON all_prices.product_id = ref_prices.product_id) as tmp
  WHERE inflation != 'NaN'::NUMERIC
) as product_inflation
ON "analysis.raw_data.product.month".product_id = product_inflation.product_id AND "analysis.raw_data.product.month".create_date = product_inflation.create_date
GROUP BY product_inflation.create_date;